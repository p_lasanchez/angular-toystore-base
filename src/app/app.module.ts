/**
 * AppModule
 * - AppComponent
 *
 * - BrowserModule
 * - FeaturesModule
 * - RouterModule
 * - HttpClientModule
 *
 * bootstrap AppComponent
 */
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";

@NgModule({
  declarations: [AppComponent],
  imports: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
