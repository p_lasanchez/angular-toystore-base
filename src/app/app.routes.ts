/**
 * app Routes
 * ----------
 *
 * /toys => ToysComponent
 *  - preload toy data with ToyResolver
 *
 * /basket => BasketComponent
 *  - inaccessible si non connecté / AuthGuard
 *
 * /autre => redirection vers ToyListComponent
 */
