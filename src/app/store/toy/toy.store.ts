/**
 * ToyStore
 *  - model : ToyState
 *
 * Actions
 *  - setToyList
 *  - setSelectedToy
 *
 *  - getError
 *  - getToyList
 *  - getSelectedToys
 *  - getToyListCount
 *  - getTotalPrice
 */
